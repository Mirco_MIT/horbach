import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';

void main() => runApp(MyApp());

const baseUrl = 'http://10.0.2.2:1337';

Future<List<Navigation>> fetchNavigation() async {
  final response =
      await http.get(baseUrl + '/navigations');

  if (response.statusCode == 200) {
    Iterable list = json.decode(response.body);
    return list.map((model) => Navigation.fromJson(model)).toList();

  } else {
    throw Exception('Failed to load nav');
  }
}

class Site {
    final int id;
    final String header;
    final String content;
    

    Site({ this.id, this.header, this.content});

    factory Site.fromJson(Map<String, dynamic> json) {
      return Site(
        id: json['id'],
        header: json['header'],
        content: json['content']
      );
    }
}

class Navigation {
  final int id;
  final String name;
  final Site site; 
  

  Navigation({ this.id, this.name, this.site});

  factory Navigation.fromJson(Map<String, dynamic> json) {
    return Navigation(
      id: json['id'],
      name: json['name'],
      site: new Site.fromJson(json['site'])
    );
  }
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Horbach',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Horbach'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<Navigation>> navList;

  @override
  void initState() {
    super.initState();
    setState(() {
      navList = fetchNavigation();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: 
           new Column(
          
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            
            children: <Widget>[FutureBuilder(
            future: navList, 
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return new Expanded(child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                         Navigation navigation = snapshot.data[index];
                         
                         return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                           children: <Widget>[
                             Text('Navigation name:  ${navigation.name}'),
                             Text('Site header:  ${navigation.site.header}'),
                             Text('Site content: ${navigation.site.content}')
                           ],
                         );
                    })
                  );
                }
                
                else {
                  return Container();
                }
            
            })
          ])
        );
  }
}
